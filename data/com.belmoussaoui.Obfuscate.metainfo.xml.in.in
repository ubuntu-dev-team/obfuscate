<?xml version="1.0" encoding="UTF-8"?>
<!-- Bilal Elmoussaoui 2019 <bilal.elmoussaoui@gnome.org> -->
<component type="desktop-application">
  <id>@app-id@</id>
  <metadata_license>CC0</metadata_license>
  <project_license>GPL-3.0-or-later</project_license>
  <name>Obfuscate</name>
  <summary>Censor private information</summary>
  <description>
    <p>Obfuscate lets you redact your private information from any image.</p>
  </description>
  <screenshots>
    <screenshot type="default">
      <image>https://gitlab.gnome.org/World/obfuscate/raw/master/data/resources/screenshots/screenshot1.png</image>
      <caption>Main Window</caption>
    </screenshot>
  </screenshots>
  <url type="homepage">https://gitlab.gnome.org/World/obfuscate/</url>
  <url type="bugtracker">https://gitlab.gnome.org/World/obfuscate/issues</url>
  <url type="translate">https://l10n.gnome.org/module/obfuscate/</url>
  <url type="vcs-browser">https://gitlab.gnome.org/World/obfuscate/</url>
  <content_rating type="oars-1.1" />
  <releases>
    <release version="0.0.10" date="2024-03-24">
      <description>
        <ul>
          <li>The application window is now adaptive</li>
          <li>Add a warning if the file is unsaved</li>
        </ul>
      </description>
    </release>
    <release version="0.0.9" date="2023-04-29">
      <description>
        <ul>
          <li>Fix crash on zoom and translations updates</li>
        </ul>
      </description>
    </release>
    <release version="0.0.8" date="2023-03-24">
      <description>
        <ul>
          <li>Various fixes and translations updates</li>
        </ul>
      </description>
    </release>
    <release version="0.0.7" date="2022-05-27">
      <description>
        <ul>
          <li>Fix a crash on open file dialog</li>
        </ul>
      </description>
    </release>
    <release version="0.0.5" date="2022-05-27">
      <description>
        <ul>
          <li>Default to the filled rectangle tool</li>
          <li>Add a warning when the blur tool is used</li>
          <li>Default to Pictures directory</li>
          <li>Translations updates &amp; bugfixes</li>
        </ul>
      </description>
    </release>
    <release version="0.0.4" date="2021-11-28">
      <description>
        <ul>
          <li>Fix opening files from a file manager</li>
          <li>Align image position to pixel grid </li>
          <li>Other bugfixes and improvements</li>
        </ul>
      </description>
    </release>
    <release version="0.0.3" date="2021-07-14">
      <description>
        <ul>
          <li>Port to GTK 4</li>
          <li>Allow opening files from the file manager</li>
          <li>Simple zooming and panning</li>
        </ul>
      </description>
    </release>
    <release version="0.0.2" date="2020-02-08">
      <description>
        <p>Bug fixes release</p>
        <ul>
          <li>Fix filter toggle</li>
          <li>Fix dropping in an image crashing the app</li>
          <li>Translations updates</li>
        </ul>
      </description>
    </release>
    <release version="0.0.1" date="2019-09-24" />
  </releases>
  <kudos>
    <!--
        GNOME Software kudos:
        https://gitlab.gnome.org/GNOME/gnome-software/blob/master/doc/kudos.md
      -->
    <kudo>ModernToolkit</kudo>
    <kudo>HiDpiIcon</kudo>
  </kudos>
  <recommends>
    <control>keyboard</control>
    <control>pointing</control>
    <control>touch</control>
  </recommends>
  <requires>
    <display_length compare="ge">360</display_length>
  </requires>
  <!-- developer_name tag deprecated with Appstream 1.0 -->
  <developer_name>Bilal Elmoussaoui</developer_name>
  <developer id="com.belmoussaoui">
    <name>Bilal Elmoussaoui</name>
  </developer>
  <update_contact>bilal.elmoussaoui@gnome.org</update_contact>
  <translation type="gettext">@gettext-package@</translation>
  <launchable type="desktop-id">@app-id@.desktop</launchable>
  <custom>
    <value key="Purism::form_factor">mobile</value>
  </custom>
</component>
