option (
  'profile',
  type: 'combo',
  choices: [
    'default',
    'development'
  ],
  value: 'default',
  description: 'The build profile for Obfuscate. One of "default" or "development".'
)
